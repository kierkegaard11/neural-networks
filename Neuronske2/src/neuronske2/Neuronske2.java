/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuronske2;

import java.util.ArrayList;
import java.util.List;
import org.neuroph.core.data.DataSet;
import org.neuroph.eval.ClassifierEvaluator;
import org.neuroph.eval.Evaluation;
import org.neuroph.eval.classification.ClassificationMetrics;
import org.neuroph.eval.classification.ConfusionMatrix;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.data.norm.MaxNormalizer;

/**
 *
 * @author Milan
 */
public class Neuronske2 {
    
    private int inputCount = 8;
    private int outputCount = 1;
    private int[] hiddenNeurons = {10, 20};
    private double[] learningRates = {0.2, 0.4, 0.6};
    List<Training> trainings = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        new Neuronske2().run();
        
    }
    
    private void run() {
        DataSet dataSet = DataSet.createFromFile("diabetes_data.csv", inputCount, outputCount, ",");
        //Normalizacija
        MaxNormalizer normalizer = new MaxNormalizer(dataSet);
        normalizer.normalize(dataSet);
        dataSet.shuffle();

        //setovi
        DataSet[] trainTestSplit = dataSet.split(0.7, 0.3);
        DataSet train = trainTestSplit[0];
        DataSet test = trainTestSplit[1];
        
        for (double lr : learningRates) {
            //iz "viseslojni perceptron" znamo da treba da koristimo Multilayer
            //posto imamo samo dva mozmo ovako da ih navedemo
            MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(inputCount, 20, 10, outputCount);
            //ako nam ne kaze momentum back propagation, stavljamo backPropagation

            BackPropagation bp = neuralNet.getLearningRule();
            bp.setMaxError(0.07);
            bp.setLearningRate(lr);
            
            neuralNet.learn(train);
            
            int numbrOfIterations = bp.getCurrentIteration();
            double accuracy = evaluateAccuracy(neuralNet, test);
            System.out.println("accuracy: " + accuracy);
            Training t = new Training(neuralNet, numbrOfIterations, accuracy);
            trainings.add(t);
        }
        printAverageIterations();
        saveNetWithMaxAcc();
    }
    
    private double evaluateAccuracy(MultiLayerPerceptron neuralNet, DataSet test) {
        Evaluation evaluation = new Evaluation();
        //da imamo vise izlaznih stavili bismo multiclass
        evaluation.addEvaluator(new ClassifierEvaluator.Binary(0.5));
        
        evaluation.evaluate(neuralNet, test);
        ClassifierEvaluator evaluator = evaluation.getEvaluator(ClassifierEvaluator.Binary.class);
        ConfusionMatrix cm = evaluator.getResult();
        System.out.println(cm.toString());
        
        ClassificationMetrics[] metrics = ClassificationMetrics.createFromMatrix(cm);
        
        ClassificationMetrics.Stats average = ClassificationMetrics.average(metrics);
        
        return average.accuracy;
    }
    
    private void printAverageIterations() {
        int iterations = 0;
        
        for (Training t : trainings) {
            iterations += t.getIterations();
        }
        System.out.println("Avg iterations: " + (double) iterations / trainings.size());
    }
    
    private void saveNetWithMaxAcc() {
        Training max = trainings.get(0);
        
        for (Training t : trainings) {
            if (t.getAccuracy() > max.getAccuracy()) {
                max = t;
            }
        }
        max.getNetwork().save("neuralNet.nnet");
    }
    
}

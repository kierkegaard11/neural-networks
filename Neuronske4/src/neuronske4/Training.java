/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuronske4;

import org.neuroph.core.NeuralNetwork;

/**
 *
 * @author Milan
 */
public class Training {

    private NeuralNetwork network;
    private double accuracy;
    private int iterations;

    public Training(NeuralNetwork network, double accuracy, int iterations) {
        this.network = network;
        this.accuracy = accuracy;
        this.iterations = iterations;
    }

    public NeuralNetwork getNetwork() {
        return network;
    }

    public void setNetwork(NeuralNetwork network) {
        this.network = network;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

}

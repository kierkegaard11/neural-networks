/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuronske4;

import java.util.ArrayList;
import java.util.List;
import org.neuroph.core.data.DataSet;
import org.neuroph.eval.ClassifierEvaluator;
import org.neuroph.eval.Evaluation;
import org.neuroph.eval.classification.ClassificationMetrics;
import org.neuroph.eval.classification.ConfusionMatrix;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.data.norm.MaxNormalizer;

/**
 *
 * @author Milan
 */
public class Neuronske4 {

    private int inputCount = 13;
    private int outputCount = 3;
    private double[] learningRates = {0.2, 0.4, 0.6};
    private List<Training> trainings = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Neuronske4().run();
    }

    private void run() {
        DataSet dataSet = DataSet.createFromFile("wines.csv", inputCount, outputCount, ",");
        MaxNormalizer normalizer = new MaxNormalizer(dataSet);
        normalizer.normalize(dataSet);
        dataSet.shuffle();

        DataSet[] trainTest = dataSet.split(0.7, 0.3);
        DataSet train = trainTest[0];
        DataSet test = trainTest[1];

        for (double lr : learningRates) {
            MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(inputCount, 22, outputCount);

            BackPropagation bp = neuralNet.getLearningRule();
            bp.setMaxError(0.02);
            bp.setLearningRate(lr);

            neuralNet.learn(train);

            double accuracy = evaluateAccuracy(neuralNet, test);
            Training t = new Training(neuralNet, accuracy, bp.getCurrentIteration());
            trainings.add(t);
        }
        printAverageIterations();
        saveMaxAccuracyNet();
    }

    private double evaluateAccuracy(MultiLayerPerceptron neuralNet, DataSet test) {
        Evaluation evaluation = new Evaluation();
        String[] names = {"s1", "s2", "s3"};
        evaluation.addEvaluator(new ClassifierEvaluator.MultiClass(names));
        evaluation.evaluate(neuralNet, test);
        ClassifierEvaluator evaluator = evaluation.getEvaluator(ClassifierEvaluator.MultiClass.class);
        ConfusionMatrix cm = evaluator.getResult();
        System.out.println(cm);
        ClassificationMetrics[] metrics = ClassificationMetrics.createFromMatrix(cm);
        ClassificationMetrics.Stats average = ClassificationMetrics.average(metrics);

        return average.accuracy;
    }

    private void saveMaxAccuracyNet() {
        Training max = trainings.get(0);
        for (Training t : trainings) {
            if (t.getAccuracy() > max.getAccuracy()) {
                max = t;
            }
        }
        max.getNetwork().save("neualNet.nnet");
    }

    private void printAverageIterations() {
        int total = 0;
        for (Training t : trainings) {
            total += t.getIterations();
        }
        System.out.println("Average iterations:" + (double) total / trainings.size());
    }

}

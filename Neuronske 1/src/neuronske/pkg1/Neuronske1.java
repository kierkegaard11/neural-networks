/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuronske.pkg1;

import java.util.ArrayList;
import java.util.List;
import org.neuroph.core.Neuron;
import org.neuroph.core.data.DataSet;
import org.neuroph.eval.ClassifierEvaluator;
import org.neuroph.eval.Evaluation;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.data.norm.MaxNormalizer;

/**
 *
 * @author Milan
 */
public class Neuronske1 {
    
    private int inputCount = 30;
    private int outputCount = 1;
    private double[] learningRates = {0.2, 0.4, 0.6};
    private int[] hiddenNeurons = {10, 20, 30};
    List<Training> trainings = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        new Neuronske1().run();
    }
    
    private void run() {
        //1. ucitavaje data seta
        System.out.println("Ucitavanje data seta");
        DataSet dataset = DataSet.createFromFile("breast_cancer_data.csv", inputCount, outputCount, ",");

        //2. normalizovati data set u odnosu na maksimalne vrednosti (max normalizacija)
        //i izmesati redosled redova u data setu
        MaxNormalizer normalizer = new MaxNormalizer(dataset);
        normalizer.normalize(dataset);
        dataset.shuffle();

        //kreirati trening i test set od 0.65 i 0.35 podataka od celog data seta
        DataSet[] trainTestSplit = dataset.split(0.65, 0.35);
        
        DataSet train = trainTestSplit[0];
        DataSet test = trainTestSplit[1];

        //istrenirati skup neuronskih mreza tipa viseslojni perceptron sa jednim skrivenim slojem neurona
        //za sve kombinacije learningRates i hiddenNeurons
        //za sve treninge koristiti algoritam momentumBackpropagation sa momentum parametrom 0.7
        //ispisati srednju vrednost broja iteracija potrebnih za trening svih mreza
        int numberOfTrainings = 0;
        int numberOfIterations = 0;
        
        for (double lr : learningRates) {
            for (int hn : hiddenNeurons) {
                MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(inputCount, hn, outputCount);
                MomentumBackpropagation bp = (MomentumBackpropagation) neuralNet.getLearningRule();
                bp.setMomentum(0.7);
                bp.setLearningRate(lr);
                bp.setMaxError(0.07);
                
                neuralNet.learn(train);
                //posle svakog treninga za tekucu neuronsku mrezu izracunati srednju kvadratnu gresku za test set
                //sami pravimo metodu

                claculateMSE(neuralNet, test);
                
                numberOfTrainings++;
                numberOfIterations += bp.getCurrentIteration();
                
            }
        }
//mrezu sa najmanjom kvadratnom greskom za test set sacuvati u fajl
        saveNeuralNetWIthMinError();
        System.out.println("Sredja vrednost broja iteracija: " + (double) numberOfIterations / numberOfTrainings);
    }
    
    private void claculateMSE(MultiLayerPerceptron neuralNet, DataSet test) {
        Evaluation e = new Evaluation();
        //kada imamo samo jedan output onda binarni klasifikator
        e.addEvaluator(new ClassifierEvaluator.Binary(0.5));
        e.evaluate(neuralNet, test);
        
        System.out.println("MSE:" + e.getMeanSquareError());
        Training t = new Training(neuralNet, e.getMeanSquareError());
        trainings.add(t);
    }
    
    private void saveNeuralNetWIthMinError() {
        double minError = 0;
        Training minTraining = trainings.get(0);
        
        for (Training t : trainings) {
            if (t.getError() < minError) {
                minError = t.getError();
                minTraining = t;
            }
        }
        
        minTraining.getNeuralNet().save("neuralNet.net");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuronske3;

import org.neuroph.core.NeuralNetwork;

/**
 *
 * @author Milan
 */
public class Training {

    private NeuralNetwork network;
    private int iterations;
    private double accuracy;

    public Training(NeuralNetwork network, int iterations, double accuracy) {
        this.network = network;
        this.iterations = iterations;
        this.accuracy = accuracy;
    }

    public NeuralNetwork getNetwork() {
        return network;
    }

    public void setNetwork(NeuralNetwork network) {
        this.network = network;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuronske3;

import java.util.ArrayList;
import java.util.List;
import org.neuroph.core.data.DataSet;
import org.neuroph.eval.ClassifierEvaluator;
import org.neuroph.eval.Evaluation;
import org.neuroph.eval.classification.ClassificationMetrics;
import org.neuroph.eval.classification.ConfusionMatrix;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.data.norm.MaxNormalizer;

/**
 *
 * @author Milan
 */
public class Neuronske3 {

    private int inputCount = 9;
    private int outputCount = 7;
    private double[] learningRates = {0.2, 0.4, 0.6};
    private int[] hiddenNeurons = {10, 20, 30};
    List<Training> trainings = new ArrayList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Neuronske3().run();
    }

    private void run() {

        DataSet dataSet = DataSet.createFromFile("glass.csv", inputCount, outputCount, ",");

        MaxNormalizer normalizer = new MaxNormalizer(dataSet);
        normalizer.normalize(dataSet);
        dataSet.shuffle();

        DataSet[] trainTestSplit = dataSet.split(0.65, 0.35);
        DataSet train = trainTestSplit[0];
        DataSet test = trainTestSplit[1];

        for (double lr : learningRates) {
            for (int hn : hiddenNeurons) {
                MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(inputCount, hn, outputCount);
                //ovo nismo radili kada imamo default
                MomentumBackpropagation bp = (MomentumBackpropagation) neuralNet.getLearningRule();
                bp.setMomentum(0.6);
                bp.setLearningRate(lr);
                //koji god broj
                bp.setMaxError(0.2);
                neuralNet.learn(train);

                double accuracy = evaluateAccuracy(neuralNet, test);

                int iterations = bp.getCurrentIteration();
                Training t = new Training(neuralNet, iterations, accuracy);
                trainings.add(t);
            }
        }

        printAverageIterations();
        saveWithMaxAccuracy();

    }

    private double evaluateAccuracy(MultiLayerPerceptron neuralNet, DataSet test) {
        Evaluation e = new Evaluation();
        //OVO JE DRUGACIJE ZBOG IZLAZA

        String[] classLabels = {"s1", "s2", "s3", "s4", "s5", "s6", "s7"};
        e.addEvaluator(new ClassifierEvaluator.MultiClass(classLabels));
        e.evaluate(neuralNet, test);

        ClassifierEvaluator evaluator = e.getEvaluator(ClassifierEvaluator.MultiClass.class);
        ConfusionMatrix cm = evaluator.getResult();
        System.out.println(cm.toString());

        ClassificationMetrics[] metrics = ClassificationMetrics.createFromMatrix(cm);

        ClassificationMetrics.Stats average = ClassificationMetrics.average(metrics);
        return average.accuracy;
    }

    private void saveWithMaxAccuracy() {
        Training max = trainings.get(0);

        for (Training t : trainings) {
            if (t.getAccuracy() > max.getAccuracy()) {
                max = t;
            }
        }
        max.getNetwork().save("neuralNet.nnet");
    }

    private void printAverageIterations() {
        int iterations = 0;

        for (Training t : trainings) {
            iterations += t.getIterations();
        }
        System.out.println("Avg iterations: " + (double) iterations / trainings.size());
    }

}
